require('dotenv').config();
const dbpg = require('../middlewares/connectionDB');
const utilitaire = require("../services/utilitaire");

//Model media
class media {
    id;
    type1;
    type2;
    designation;
    lien;
    etat;

    constructor(id,type1,type2,designation,lien,etat) {
      this.id = id;
      this.type1 = type1;
      this.type2 = type2;
      this.designation = designation;
      this.lien = lien;
      this.etat = etat;
    }

    async insert(){
        const clientdb = await dbpg.connect()
        var resultat = [];
        try {
            await clientdb.query('BEGIN')
            //generation d'id
            var idZone = "MD";
            idZone = idZone+utilitaire.genIdNumber((await clientdb.query("SELECT nextval('seqmedia')")).rows[0].nextval);
            this.id = idZone;
            //insertion 
            const insertText = "INSERT INTO media VALUES ($1,$2,$3,$4,$5,$6)";
            const insertVal = [this.id,
                                this.type1,
                                this.type2,
                                this.designation,
                                this.lien,
                                process.env.ETAT_CREER];
            resultat = await clientdb.query(insertText,insertVal);
            await clientdb.query('COMMIT')
          } catch (e) {
            await clientdb.query('ROLLBACK')
            throw e
          } finally {
            await clientdb.release()
          }
          return resultat;
    }
}

exports.media = media;